package booksexercise.controllers;

import org.omg.CosNaming.NamingContextPackage.NotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import booksexercise.entities.Book;
import booksexercise.services.BooksService;

@RestController
public class BooksRestController {
	
	@Autowired
	private BooksService booksService;
	 
//====================================================
// 				POST /books/echo
//====================================================
	@RequestMapping( 
			path="/books/echo",
			method=RequestMethod.POST,
			produces=MediaType.APPLICATION_JSON_VALUE,
			consumes=MediaType.APPLICATION_JSON_VALUE)
	public Book echo(@RequestBody Book book) throws Exception{
		return booksService.addBook(book);
	}
	
//====================================================
//				GET /books/allBooks
//====================================================	
	@RequestMapping(
			path="/books/allBooks",
			method=RequestMethod.GET,
			produces=MediaType.APPLICATION_JSON_VALUE)
	public Book[] getAllBooks() {
		 return booksService.getAllBooks();
	}
	
//====================================================
//		GET /books/all?content=isbn/title/details
//====================================================		
	@RequestMapping(
			path="/books/all",
			method=RequestMethod.GET,
			produces=MediaType.APPLICATION_JSON_VALUE)
	public Object[] getAll(@RequestParam(name="content") String content,
			@RequestParam(name="size", required=false, defaultValue="20") int size,
			@RequestParam(name="page", required=false, defaultValue="0") int page)
					throws NullPointerException, IllegalArgumentException {
		
		if(content == null) {
			throw new NullPointerException("error from function all: The content can not be null.");
		}
		
		if(content.equalsIgnoreCase("isbn")) {
			return booksService.getAllBooksIsbn(size, page);
			
		} else if (content.equalsIgnoreCase("title")) {
			return booksService.getAllBooksTitle(size, page);
			
		} else if (content.equalsIgnoreCase("detailed")) {
			return booksService.getAllBooksDetails(size, page);		
			
		} else {
			throw new IllegalArgumentException("error from function all: The content is not valid value.");
			
		}
		
	}
	
//====================================================
//				GET /byIsbn/{isbn}
//====================================================		
	@RequestMapping(
			path="/books/byIsbn/{isbn}",
			method=RequestMethod.GET)
	public Book getBookByIsbn(@PathVariable("isbn") String isbn) throws NotFound{
		 return booksService.getBookByIsbn(isbn);
	}
	
//====================================================
//	DELETE /books
//====================================================	
	@RequestMapping(
			path="/books/",
			method=RequestMethod.DELETE)
	public void delete(){
		booksService.deleteAllBooks();
	}
	
	@ExceptionHandler
	@ResponseStatus(value=HttpStatus.NO_CONTENT)
	public String handleException (NullPointerException e) {
		e.printStackTrace();
		return e.getMessage();
	}
	
	@ExceptionHandler
	@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR)
	public String handleException (IllegalArgumentException e) {
		e.printStackTrace();
		return e.getMessage();
	}
	
	@ExceptionHandler
	@ResponseStatus(value=HttpStatus.NOT_FOUND)
	public String handleException (NotFound e) {
		e.printStackTrace();
		return "The book wasn't found.";
	}
	
	@ExceptionHandler
	@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR)
	public String handleException (RuntimeException e) {
		e.printStackTrace();
		return e.getMessage();
	}
}
