package booksexercise.controllers;

import booksexercise.entities.LoanDetails;
import booksexercise.services.LoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/loans")
public class LoansController {

    @Autowired
    private LoanService loanService;

    @PostMapping(path = "/{isbn}")
    public LoanDetails loanBookByIsbn(@PathVariable String isbn){
        return null;
    }

    @PutMapping(path = "/{loanId}")
    public LoanDetails returnBookByLoanId(@PathVariable String loanId){
        return null;
    }

    @GetMapping(path = "/{loanId}")
    public LoanDetails getLoanByLoanId(@PathVariable String loanId){
        return null;
    }

    @GetMapping
    public List<LoanDetails> getAllLoans(@PathVariable String loanId){
        return null;
    }

    @GetMapping(path = "/isbn")
    public LoanDetails getLoanByIsbn(@RequestParam String isbn){
        return null;
    }

    @GetMapping(path = "/status")
    public List<LoanDetails> getAllReturnedLoans(@RequestParam String status){
        return null;
    }

    @DeleteMapping
    public void deleteAllLoans(){

    }


}
