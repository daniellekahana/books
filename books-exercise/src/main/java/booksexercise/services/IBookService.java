package booksexercise.services;


import org.omg.CosNaming.NamingContextPackage.NotFound;
import booksexercise.entities.Book;

public interface IBookService {

	Book addBook(Book book) throws Exception;
	String[] getAllBooksIsbn(int size, int page) ;
	String[] getAllBooksTitle(int size, int page);
	Book[] getAllBooksDetails(int size, int page);
	Book[] getAllBooks();
	Book getBookByIsbn(String isbn) throws NullPointerException, NotFound;
	void deleteAllBooks();
	
	
}

