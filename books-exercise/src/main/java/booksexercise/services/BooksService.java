package booksexercise.services;

import java.util.stream.StreamSupport;
import org.omg.CosNaming.NamingContextPackage.NotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import booksexercise.dal.BooksDAO;
import booksexercise.entities.Book;

@Service
public class BooksService implements IBookService {
	
	private BooksDAO booksDao;
	
	public BooksService() {
	}

	@Autowired
	public BooksService(BooksDAO booksDao) {
		this.booksDao = booksDao;
	}
	
	@Override
	@Transactional
	public Book addBook(Book book) throws Exception{
		
		if(book != null && book.getIsbn() != null) {
			if (!booksDao.existsById(book.getIsbn())) {
				booksDao.save(book);
				return book;
			}
			else {
				throw new RuntimeException("Book already exist");
			}
		}
		throw new NullPointerException("error from function add book: Book is null.");
	}
	
	@Override
	@Transactional(readOnly=true)
	public String[] getAllBooksIsbn(int size, int page) {
		return this.booksDao.findAll(PageRequest.of(page, size, Direction.ASC, "isbn"))
				.getContent().stream().map(book->book.getIsbn()).toArray(String[]::new);	
				
	}
	
	@Override
	@Transactional(readOnly=true)
	public String[] getAllBooksTitle(int size, int page) {
		return this.booksDao.findAll(PageRequest.of(page, size, Direction.ASC, "isbn"))
				.getContent().stream().map(book->book.getTitle()).toArray(String[]::new);
	}
	
	@Override
	@Transactional(readOnly=true)
	public Book[] getAllBooksDetails(int size, int page) {
		return this.booksDao.findAll(PageRequest.of(page, size, Direction.ASC, "isbn"))
				.getContent().toArray(new Book[0]);
	}
	
	@Override
	@Transactional(readOnly=true)
	public Book getBookByIsbn(String isbn) throws NullPointerException, NotFound {
		if(isbn == null) {
			throw new NullPointerException("error from function getBookByIsbn: Isbn is null.");
			
		} else if (isbn.length() < 10 || isbn.length() > 13) {
			throw new IllegalArgumentException("The isbn "+isbn+" is not between 10 to 13 chars.");
			
		}
		
		return booksDao.findById(isbn).orElseThrow(() -> new NotFound());
	}
	
	@Override
	@Transactional
	public void deleteAllBooks() {
		booksDao.deleteAll();
	}

	@Override
	public Book[] getAllBooks() {
		return StreamSupport.stream(this.booksDao.findAll().spliterator(), false).toArray(Book[]::new);
	}
	

}
