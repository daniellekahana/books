package booksexercise.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;

@Entity
@Table(name = "BOOK")
public class Book {
	private String isbn;
	private String title;
	private Publisher publisher;
	private String[] authors;
	private int publishedYear;
	private int rating;
	
	public Book() {
		isbn = null;
		title = null;
		publisher = null;
		authors = null;
		publishedYear = 0;
		rating = 0;
	}

	@Autowired
	public Book(String isbn, String title, Publisher publisher, String[] authors, int publishedYear, int rating) {
		setIsbn(isbn);
		setTitle(title);
		setAuthors(authors);
		setPublisher(publisher);
		setPublishedYear(publishedYear);
		setRating(rating);
	}

	@Id
	public String getIsbn() {
		return isbn;
	}
	
	public void setIsbn(String isbn) throws IllegalArgumentException{
		if(isbn.length() < 10 || isbn.length() > 13) {
			throw new IllegalArgumentException("The isbn has to contain between 10 to 13 chars");
		}
		this.isbn = isbn;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}

	public void setAuthors(String[] authors) {
		this.authors = authors;
	}
	
	public String[] getAuthors() {
		return authors;
	}
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	public Publisher getPublisher() {
		return publisher;
	}
	
	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
	}
	
	public int getPublishedYear() {
		return publishedYear;
	}
	
	public void setPublishedYear(int publishedYear) {
		this.publishedYear = publishedYear;
	}
	
	public int getRating() {
		return rating;
	}
	
	public void setRating(int rating) throws IllegalArgumentException{
		if(rating < 0 || rating > 5) {
			throw new IllegalArgumentException("The rating has to be between 0 to 5");
		}
		this.rating = rating;
	}	

}
