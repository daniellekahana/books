package booksexercise.dal;

import org.springframework.data.repository.CrudRepository;

import booksexercise.entities.Publisher;

public interface PublisherDAO extends CrudRepository<Publisher, String>{

}
