package booksexercise.dal;

import org.springframework.data.repository.PagingAndSortingRepository;

import booksexercise.entities.Book;

public interface BooksDAO extends PagingAndSortingRepository<Book, String>{

}
