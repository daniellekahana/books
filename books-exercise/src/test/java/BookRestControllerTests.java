import static org.assertj.core.api.Assertions.assertThat;

import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import booksexercise.BooksApplication;
import booksexercise.entities.Book;
import booksexercise.services.IBookService;

//https://spring.io/guides/gs/testing-web/

@RunWith(SpringRunner.class)
@SpringBootTest(classes = BooksApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class BookRestControllerTests {

	@LocalServerPort
	private int port;
	private String restBaseUrl;
	private RestTemplate restTemplate;


	@Autowired
	private IBookService bookService;

	@PostConstruct
	public void init() {
		this.restTemplate = new RestTemplate();
		this.restBaseUrl = "http://localhost:" + port + "/books/";
	}

	@Before
	public void setup() {
	}

	@After
	public void teardown() {
		this.bookService.deleteAllBooks();
	}

	@Test
	public void testServerIsBootingCorrectly() throws Exception {
	}

	
//====================================================
//		POST /books/echo
//====================================================
	
	@Test
	public void addBookSuccessfully() throws Exception {
		String isbn = "book_isbn_1";
		Book book = new Book();
		book.setIsbn(isbn);
		// given database is empty
		// when post book with isbn:book_isbn_1
		bookService.addBook(book);
		
		//retrieve book from database
		assertThat(this.bookService.getBookByIsbn(isbn))
			.isNotNull()
			.extracting("isbn")
			.containsExactly(isbn);
	}

	@Test(expected = Exception.class)
	public void addAlreadyExistingBook() throws Exception {
		String url = restBaseUrl + "echo/";
		String isbn = "book_isbn_1";

		// given the following book in the database
		Book book = new Book();
		book.setIsbn(isbn);
		this.bookService.addBook(book);

		// when posting Book with same isbn
		Book postBook = new Book();
		postBook.setIsbn(isbn);

		// expected to throw exception
		this.restTemplate.postForObject(url, postBook, Book.class);
	}

	@Test(expected = Exception.class)
	public void addBookWithInvalidIsbnLength() throws Exception {
		String url = restBaseUrl + "echo/";
		String isbn = "short";

		// given database is empty
		// when post book with invalid isbn length
		Book book = new Book();
		book.setIsbn(isbn);

		// expected to throw exception
		this.restTemplate.postForObject(url, book, Book.class);
	}

	
//====================================================
//	GET /books/all?content=isbn/title/detailed
//====================================================
	
	@Test(expected=Exception.class)
	public void testGetAllBooksWithFaultPaginationParameters() throws Exception{
		String url = restBaseUrl + "all?content=isbn&size={maxSize}&page={page}";
		String isbn = "book_isbn_1";
		
		// given a book
		Book book = new Book();
		book.setIsbn(isbn);

		// given the database is empty
		
		// when - GET
		this.restTemplate.getForObject(url, Book[].class, isbn, -1);
		
		// then response <> 2xx
	}
	
	@Test
	public void testGetAllBooksByIsbn() throws Exception {
		String url = restBaseUrl + "all?content=isbn";
		String isbn1 = "book_isbn_1";
		String isbn2 = "book_isbn_2";
		String isbn3 = "book_isbn_3";
		
		Book book1 = new Book();
		book1.setIsbn(isbn1);
		Book book2 = new Book();
		book2.setIsbn(isbn2);
		Book book3 = new Book();
		book3.setIsbn(isbn3);
		
		
		this.bookService.addBook(book1);
		this.bookService.addBook(book2);
		this.bookService.addBook(book3);
		
		String[] allBooksIsbn = this.restTemplate.getForObject(url, String[].class);
		
		assertThat(allBooksIsbn).isNotNull().hasSize(3).containsExactly(isbn1,isbn2,isbn3);
		
	}
	
	
//====================================================
//	GET /byIsbn/{isbn}
//====================================================	
	
	
	@Test
	public void getBookByIsbnSuccessfully() throws Exception {
		String url = restBaseUrl + "byIsbn/";
	
		// given a book {isbn:book_isbn_1, title:test}
		String isbn = "book_isbn_1";
		String title = "test";
		Book book = new Book();
		book.setIsbn(isbn);
		book.setTitle(title);
		
		// given the database contains { "isbn": "book_isbn_1", "title":"test"}
		bookService.addBook(book);

		// when - GET
		Book actualBook = this.restTemplate.getForObject(url + "{isbn}",Book.class, isbn);

		// then
		assertThat(actualBook)
			.isNotNull()
			.extracting("isbn", "title")
			.containsExactly(isbn,title);
	}
	
	
//====================================================
//	DELETE /books
//====================================================	
	
	//TODO delete method
	//second commit
	//third commit
	public void foo() {
		System.out.println("Go Team Jenkins!!1");
		
	}
	
	public void goo2() {
		System.out.println("Go Team Jenkins!");
	}
	
	
}
